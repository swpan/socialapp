<?php
session_start();
require_once("database.php");
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<style>
    .home_header {
        background-color: black;
        color: white;
    }

    nav a::hover {
        background-color: blue;
    }

    .comment-form {
        display: none;
    }
</style>
<body>

<div class="home_header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="images/img2.png" height="50" width="50">
                <input type="search" value="search" size="60">
            </div>
            <div class="col-md-6">

                <nav class="nav nav-pills flex-column flex-sm-row">
                    <a class="flex-sm-fill text-sm-center nav-link" href="pprofile.php">Profile</a>
                    <a class="flex-sm-fill text-sm-center nav-link" href="home.php">Home</a>
                    <a class="flex-sm-fill text-sm-center nav-link" href="logout.php">Log Out</a>
                </nav>
            </div>


        </div>
    </div>
</div>
<div class="home_body">
    <div class="row">
        <div class="col-md-3">
            <h1></h1>
        </div>
        <div class="col-md-6">
            <p></p>
            <form id="addform" enctype="multipart/form-data">
                <textarea cols="60" rows="5" name="body" id="name"></textarea>
                <p></p>
                <input type="file" name="img" id="img">
                <p></p>
                <input type="button" name="sub" class="btn btn-success" id="addpost" value="Add Post">
            </form>

            <div id="showdata">
                <div id="showcomment"></div>
            </div>


        </div>
        <div class="col-md-3">
        </div>
    </div>
</div>


<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>


<script>
    $(document).ready(function () {

        //........................like start..................
        $("#showdata").on("click", "#likepost", function () {

            var postid = $(this).data('postid');
            var userid = $(this).data('userid');
            //alert(postid +"-"+ userid);
            $.post("classes/addlike.php",
                {
                    postid: postid,
                    user: userid,
                    action: 'liked'
                },
                function (d) {


                });
        });
        //...........................like end.........................

        //...........................comment end.........................

        $("#showdata").on("click", ".comment-open", function () {

            var postid = ($(this).attr("data-postid"));
            var userid = ($(this).attr("data-userid"));
            $("#commentform-" + postid).toggle();

        });
        $("#showdata").on("click", "#subComment", function () {
            //alert("hi");
            var commentValue = $(this).parent().find('textarea').val();

            var postid = $(this).attr("data-postid");
            var userid = $(this).attr("data-userid");
            //console.log(postid+":"+userid + ":"+commentValue);
            $.post(
                'classes/addcomment.php',
                {
                    postid: postid,
                    userid: userid,
                    commentPost: commentValue,
                    action: 'comment'
                },
                function (d) {
                    //console.log(d);
                    d = JSON.parse(d);
                    console.log(d);
                });
        });
        //...........................comment end.........................

        function showpost(obj) {
            var show = "<div><p></p><div><img src='images/" + obj.postimg + "'width='400' height='400'/></div><p>" + obj.body + "</p><p></p><div><a href='#' class='btn btn-outline-primary'>Like</a> <a href='#' class='btn btn-outline-primary'>Comment</a> <a href='#' class='btn btn-outline-primary'>Share</a></div><p></p><input type='text' width='90%'/><hr></div>";
            $("#showdata").prepend(show);
        }


        function allpost() {
            $.get("classes/allposts.php", function (d) {
                $("#showdata").html(d);
            });
        }

        $("#addpost").click(function (e) {
            //alert(5);
            e.preventDefault();
            var formData = new FormData();
            formData.append("postcontent", $("#name").val());
            formData.append("postimage", $('input[type=file]')[0].files[0]);
            formData.append("action", "addpost");
            $.ajax({
                url: 'classes/post.php',
                data: formData,
                type: "POST", //ADDED THIS LINE
                contentType: false,
                processData: false,
                success: function (data) {
                    console.log(data);

                    //showpost(data);
                    //$("#name").val(" ");
                    //$("#img").val(" ");
                    $("#showdata").html(data);


                }
            });


        });
        //call allpost when the page load complete
        allpost();





    });

</script>


</body>
</html>