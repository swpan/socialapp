/**
 * Created by ASUS on 12/22/2017.
 */
$(function (e) {
    // alert('working');
    $(".editable").each(function (e) {
        var fieldvalue = $(this).find("span.value").text();
        $(this).find("input.fieldName").val(fieldvalue);
    })

    $(".edit").each(function (e) {

        $(this).click(function (e) {
            var fieldval = $(this).parents(".editable").find("span.value").text();

            $(this).parents(".editable").find("input.fieldName").val(fieldval);

            $(this).parents(".editable").siblings().find('.info-form').hide();
            $(this).parents(".editable").find('.info-form').toggle();
        })
    })


    $(".update").each(function (e) {
        $(this).click(function (e) {
            $(this).parents(".editable").find("span.value").html('<img src="assets/images/loader.svg">');
            var fieldName = $(this).parents(".editable").find("input.fieldName").val();
            var fieldKey = $(this).parents(".editable").find("input.fieldKey").val();
            // alert(fieldKey);
            
            ajaxUpdate(fieldName, fieldKey, 10);

            $(this).parents(".editable").find("span.value").delay( 999800 ).html(fieldName);
        })
    })


})

function ajaxUpdate(field, key, userid) {
    $.post('ajaxUpdate.php', {fieldName: field, fieldKey: key}, function (data) {
        console.log(data);
    })
}