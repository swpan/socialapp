-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2017 at 06:46 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `swpan_social`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `posted_at` datetime NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `posted_at`, `post_id`) VALUES
(1, 'sdfads', 5, '2017-12-10 03:05:56', 20),
(2, 'Et nihil asperiores rerum quam officia', 5, '2017-12-10 03:14:10', 20),
(3, 'Qui in soluta id saepe non et odio est', 5, '2017-12-10 04:10:32', 20),
(4, 'Eius sit dolorum et corporis eius quia voluptas quis quod excepteur sed qui et corporis dolor dolor fuga Molestiae ut', 5, '2017-12-10 04:17:33', 20),
(5, 'Vel vel adipisicing deserunt dolore voluptatem culpa dignissimos quia eum commodo', 5, '2017-12-10 04:29:35', 20),
(6, 'Eum a lorem omnis ad repudiandae voluptas Nam numquam quam voluptatum dolor', 5, '2017-12-10 04:31:29', 20),
(7, 'Voluptatibus commodi voluptate doloremque omnis illum sed nulla enim corrupti deserunt', 5, '2017-12-10 04:33:29', 19),
(8, 'Molestiae harum officia dolorum aut', 5, '2017-12-10 04:34:41', 20),
(9, 'Dicta tempore ex officia Nam amet libero ut sunt minus adipisci et possimus iure minus maiores provident', 5, '2017-12-10 04:36:56', 20),
(10, 'Sed quisquam dolor culpa aliquam', 5, '2017-12-10 04:37:56', 20),
(11, 'Doloremque officia ullam esse sit sunt eum omnis quo quia non', 5, '2017-12-10 04:38:23', 20),
(12, 'sadfads', 5, '2017-12-12 00:33:44', 20),
(13, '', 5, '2017-12-22 20:17:37', 23);

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `follower_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_tokens`
--

CREATE TABLE `login_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `token` char(64) NOT NULL DEFAULT '',
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_tokens`
--

INSERT INTO `login_tokens` (`id`, `token`, `user_id`) VALUES
(2, 'b73e900574a7599025b934a458951e9d59e02b9d', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `body` text NOT NULL,
  `sender` int(11) UNSIGNED NOT NULL,
  `receiver` int(11) UNSIGNED NOT NULL,
  `read` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` int(11) UNSIGNED NOT NULL,
  `receiver` int(10) UNSIGNED NOT NULL,
  `sender` int(11) UNSIGNED NOT NULL,
  `extra` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_tokens`
--

CREATE TABLE `password_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `token` char(64) NOT NULL DEFAULT '',
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `body` varchar(160) NOT NULL DEFAULT '',
  `posted_at` datetime NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `likes` int(11) UNSIGNED NOT NULL,
  `postimg` varchar(255) DEFAULT NULL,
  `topics` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `body`, `posted_at`, `user_id`, `likes`, `postimg`, `topics`) VALUES
(14, 'Sed in cillum dolore tempore dolor', '2017-12-09 02:23:38', 5, 0, '5_1512764618_35100.jpg', ''),
(15, 'Sed in cillum dolore tempore dolor', '2017-12-09 02:23:46', 5, 0, '5_1512764626_39730.jpg', ''),
(16, 'Amet velit possimus praesentium iusto duis ab minima accusamus veniam fuga Id dolor velit culpa quos non labore corrupti placeat', '2017-12-09 02:26:30', 5, 1, '5_1512764790_27477.jpg', ''),
(17, 'At ut incididunt accusantium labore fuga Est ut cupidatat id recusandae Sed est non in magnam laborum Qui', '2017-12-09 02:27:36', 5, 0, '5_1512764856_34985.jpg', ''),
(18, 'Veritatis est non laboris nihil eveniet optio proident exercitation eos in et est et aut sed quis incididunt nemo', '2017-12-09 02:28:45', 5, 0, '5_1512764925_26916.jpg', ''),
(19, 'Voluptate qui veniam dolore elit voluptatum in dolor debitis impedit dolorem et porro explicabo Repudiandae', '2017-12-09 02:29:45', 5, 0, '5_1512764985_15973.jpg', ''),
(20, 'Facilis alias adipisci voluptatem non reiciendis ut voluptas voluptatem sed numquam molestiae alias', '2017-12-09 03:07:26', 5, 0, '5_1512767246_17718.jpg', ''),
(22, 'Commodo delectus quod ut nihil ullam dolor elit consequuntur inventore in dolore blanditiis officia reprehenderit sunt eum ea sunt', '2017-12-12 03:43:44', 5, 0, '5_1513028624_33140.jpg', ''),
(23, 'Quia velit consequuntur aut fugit', '2017-12-12 03:44:13', 5, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

CREATE TABLE `post_likes` (
  `id` int(11) UNSIGNED NOT NULL,
  `post_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_likes`
--

INSERT INTO `post_likes` (`id`, `post_id`, `user_id`) VALUES
(1, 20, 5),
(2, 19, 5),
(3, 18, 5),
(4, 16, 5);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sur_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bgroup` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` set('m','f','c') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `sur_name`, `dob`, `phone`, `bgroup`, `gender`, `created`, `deleted`) VALUES
(1, 3, 'abu', 'mamun', '2017-11-01', '01911123456', 'b+', 'm', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 4, 'fg', 'fdgfd', '2017-11-23', '11235', '', 'f', '2017-11-21 10:57:41', '0000-00-00 00:00:00'),
(3, 5, 'ashrafun', 'swapna', '2017-12-03', '02546545', 'A+', 'f', '2017-12-05 10:41:24', '0000-00-00 00:00:00'),
(4, 6, 'Abir', 'Masfiq', '0000-00-00', '32123', 'o+', 'm', '2017-12-11 20:34:55', '0000-00-00 00:00:00'),
(5, 7, 'Abir', 'Masfiq', '0000-00-00', '32123', 'o+', 'm', '2017-12-11 20:35:34', '0000-00-00 00:00:00'),
(6, 8, 'Abir', 'Mashfiq', '0000-00-00', '126325', '-1', 'm', '2017-12-11 20:36:37', '0000-00-00 00:00:00'),
(7, 9, 'Abir', 'Mashfiq', '0000-00-00', '126325', '-1', 'm', '2017-12-11 20:37:14', '0000-00-00 00:00:00'),
(8, 10, 'Abir', 'Mashfiq', '0000-00-00', '563785', 'o+', 'm', '2017-12-11 20:38:26', '0000-00-00 00:00:00'),
(9, 11, 'Abir', 'Mashfiq', '0000-00-00', '563785', 'o+', 'm', '2017-12-11 20:39:03', '0000-00-00 00:00:00'),
(10, 12, 'Abir', 'Mashfiq', '0000-00-00', '563785', 'o+', 'm', '2017-12-11 20:39:05', '0000-00-00 00:00:00'),
(11, 13, 'Abir', 'Mashfiq', '0000-00-00', '563785', 'o+', 'm', '2017-12-11 20:39:09', '0000-00-00 00:00:00'),
(12, 14, 'Abir', 'Mashfiq', '0000-00-00', '563785', 'o+', 'm', '2017-12-11 20:41:22', '0000-00-00 00:00:00'),
(13, 15, '', '', '2005-07-13', '', '-1', '', '2017-12-11 20:52:29', '0000-00-00 00:00:00'),
(14, 16, 'abir', 'abir', '0000-00-00', '6545', '', '', '2017-12-11 21:02:31', '0000-00-00 00:00:00'),
(15, 17, 'abir', 'abir', '0000-00-00', '656', '', '', '2017-12-11 21:03:14', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE `profile_pic` (
  `id` int(5) NOT NULL,
  `profile_pic` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(5) NOT NULL,
  `create_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profile_pic`
--

INSERT INTO `profile_pic` (`id`, `profile_pic`, `user_id`, `create_by`) VALUES
(47, '5_1513766508_20137.jpg', 5, '2017-12-20 10:41:48'),
(48, '5_1513766512_23212.jpg', 5, '2017-12-20 10:41:52'),
(49, '5_1513766676_35906.jpg', 5, '2017-12-20 10:44:36'),
(50, '5_1513766677_44840.jpg', 5, '2017-12-20 10:44:37'),
(51, '5_1513766813_27540.jpg', 5, '2017-12-20 10:46:53'),
(52, '5_1513766814_23317.jpg', 5, '2017-12-20 10:46:54'),
(53, '5_1513766814_11612.jpg', 5, '2017-12-20 10:46:54'),
(54, '5_1513766815_37838.jpg', 5, '2017-12-20 10:46:55'),
(55, '5_1513766911_35450.jpg', 5, '2017-12-20 10:48:31'),
(56, '5_1513766924_30798.jpg', 5, '2017-12-20 10:48:44'),
(57, '5_1513766927_14312.jpg', 5, '2017-12-20 10:48:47'),
(58, '5_1513767526_17884.jpg', 5, '2017-12-20 10:58:46'),
(59, '5_1513767530_32348.jpg', 5, '2017-12-20 10:58:50'),
(60, '5_1513767533_37750.jpg', 5, '2017-12-20 10:58:53'),
(61, '5_1513767990_42379.jpg', 5, '2017-12-20 11:06:30'),
(62, '5_1513767999_14988.jpg', 5, '2017-12-20 11:06:39'),
(63, '5_1513768585_20416.jpg', 5, '2017-12-20 11:16:25'),
(64, '5_1513768591_18650.jpg', 5, '2017-12-20 11:16:31'),
(65, '5_1513768738_17827.jpg', 5, '2017-12-20 11:18:58'),
(66, '5_1513769251_15965.jpg', 5, '2017-12-20 11:27:31'),
(67, '5_1513769257_33467.jpg', 5, '2017-12-20 11:27:37'),
(68, '5_1513769350_23512.jpg', 5, '2017-12-20 11:29:10'),
(69, '5_1513769428_28414.jpg', 5, '2017-12-20 11:30:28'),
(70, '5_1513769436_44299.jpg', 5, '2017-12-20 11:30:36'),
(71, '5_1513769440_27631.jpg', 5, '2017-12-20 11:30:40'),
(72, '5_1513769500_24368.jpg', 5, '2017-12-20 11:31:40'),
(73, '5_1513769508_42922.jpg', 5, '2017-12-20 11:31:48'),
(74, '5_1513769566_43836.jpg', 5, '2017-12-20 11:32:46'),
(75, '5_1513769573_11912.jpg', 5, '2017-12-20 11:32:53'),
(76, '5_1513769654_25751.jpg', 5, '2017-12-20 11:34:14'),
(77, '5_1513769659_25856.jpg', 5, '2017-12-20 11:34:19'),
(78, '5_1513769661_36454.jpg', 5, '2017-12-20 11:34:21'),
(79, '5_1513769732_22273.jpg', 5, '2017-12-20 11:35:32'),
(80, '5_1513769736_39717.jpg', 5, '2017-12-20 11:35:36'),
(81, '5_1513769737_13298.jpg', 5, '2017-12-20 11:35:37'),
(82, '5_1513770134_43744.jpg', 5, '2017-12-20 11:42:14'),
(83, '5_1513771565_41033.jpg', 5, '2017-12-20 12:06:05'),
(84, '5_1513772281_23193.jpg', 5, '2017-12-20 12:18:01'),
(85, '5_1513772310_48229.jpg', 5, '2017-12-20 12:18:30'),
(86, '5_1513773084_38470.jpg', 5, '2017-12-20 12:31:24'),
(87, '5_1513952798_36631.jpg', 5, '2017-12-22 14:26:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `email` text,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `profileimg` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `verified`, `profileimg`) VALUES
(1, 'idbbisew', '$2y$10$BgxuOBy8d5piCJa4FD93EOvGikX9P3ITXNU37ii3OgGWV6kVkRw3a', 'idbbisew@gmail.com', 1, ''),
(3, 'mamun@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', 'mamun@gmail.com', 1, ''),
(4, 'fdd@ghs.dfh', '$2y$10$eJyxuMddsjAv7YTLPGrrBOqy6GviupeQtnZzoGF50PCikVbY5pTLa', 'fdd@ghs.dfh', 1, ''),
(5, 'swpori@gmail.com', '$2y$10$5dxj2QRjuIFWcoFPIMLvyex/Wt9BurWdDgp1QTeNjlD6w0Cq4WMEa', 'swpori@gmail.com', 1, ''),
(16, 'abir@gmail.com', '$2y$10$TqvD7qRIyvsGdvnWi25dsOBRQ35KaodCbwA.bQRBHIjpxyt5szk3a', 'abir@gmail.com', 1, ''),
(17, 'abir@gmail.com', '$2y$10$mNcK.rqM7OHfBZfrlToR5uKdlOEAX2nRimQMxzgjMVokjUD5viLPu', 'abir@gmail.com', 1, ''),
(18, 'aanhsn@gmail.com', '61bdee065081eb3f432fea675e239e60', 'aanhsn@gmail.com', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `user_id` int(5) NOT NULL,
  `user_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_workplace` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_school` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_college` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_university` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_living_place` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_marital_status` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_birthday` datetime NOT NULL,
  `about_user` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `user_id`, `user_name`, `user_workplace`, `user_school`, `user_college`, `user_university`, `user_living_place`, `user_marital_status`, `user_birthday`, `about_user`, `created_time`, `updated_time`) VALUES
(1, 18, 'Ashrafun Nahera', 'IDBISAW aas', 'Badshah Faisal Institute', 'Dhaka Polytechnic Institute ', 'BUBT', 'Banasree', 'Married', '1986-01-01 00:00:00', 'I am hasan', '2017-12-23 08:46:30', '2017-12-23 08:46:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_tokens`
--
ALTER TABLE `login_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_tokens`
--
ALTER TABLE `password_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_tokens`
--
ALTER TABLE `login_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_tokens`
--
ALTER TABLE `password_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `post_likes`
--
ALTER TABLE `post_likes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `login_tokens`
--
ALTER TABLE `login_tokens`
  ADD CONSTRAINT `login_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD CONSTRAINT `post_likes_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
