<?php
session_start();
require_once("database.php");
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>About</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<style>
    .home_header {
        background-color: black;
        color: white;
    }

    nav a:hover {
        background-color: blue;
    }

    .comment-form {
        display: none;
    }

</style>

<body>

<div class="home_header">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="images/img2.png" height="50" width="50">
                <input type="search" value="search" size="60">
            </div>
            <div class="col-md-6">

                <nav class="nav nav-pills flex-column flex-sm-row">
                    <a class="flex-sm-fill text-sm-center nav-link" href="profile.php">Profile</a>
                    <a class="flex-sm-fill text-sm-center nav-link" href="home.php">Home</a>
                    <a class="flex-sm-fill text-sm-center nav-link" href="logout.php">Log Out</a>
                </nav>
            </div>
        </div>

    </div>
</div>
<div class="container">
    <img src="images/daisy.jpg" height="300" width="100%">


    <div class="row">
        <div class="col-sm-3">

            <!--p><a href="">About</a></p-->
        </div>
        <div class="col-sm-7"></div>
        <div class="col-sm-2"></div>
    </div>

    <div class="content-area">
        <h1>About <?php ?></h1>
    </div>

</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script>


</script>


</body>

</html>
