<?php
session_start();
require_once( "database.php" );
?>
<!DOCTYPE html>

<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<style>
  .home_header {
    background-color: black;
    color: white;
  }

  nav a:hover {
    background-color: blue;
  }

  .comment-form {
    display: none;
  }

  .info-form {
    position: absolute;
    top: 0;
    right: 0;
    display: none;
  }
  .value img{position: absolute;top: calc(50% - 40px);}
</style>

<body>

<div class="home_header">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="images/img2.png" height="50" width="50">
        <input type="search" value="search" size="60">
      </div>
      <div class="col-md-6">

        <nav class="nav nav-pills flex-column flex-sm-row">
          <a class="flex-sm-fill text-sm-center nav-link" href="profile.php">Profile</a>
          <a class="flex-sm-fill text-sm-center nav-link" href="home.php">Home</a>
          <a class="flex-sm-fill text-sm-center nav-link" href="logout.php">Log Out</a>
        </nav>
      </div>
    </div>

  </div>
</div>
<div class="container">
  <img src="images/daisy.jpg" height="300" width="100%">


  <div class="nav-tab">
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
           aria-controls="pills-home" aria-selected="true">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
           aria-controls="pills-profile" aria-selected="false">Timeline</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab"
           aria-controls="pills-contact" aria-selected="false">Contact</a>
      </li>
    </ul>

  </div>
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
		<?php
		$selectQuery       = "select * from user_info";
		$selectQueryResult = $conn->query( $selectQuery );
		while ( $row = $selectQueryResult->fetch_array() ) {
			$userInfo                        = array();
			$userInfo['user_name']           = $row['user_name'];
			$userInfo['user_workplace']      = $row['user_workplace'];
			$userInfo['user_school']         = $row['user_school'];
			$userInfo['user_college']        = $row['user_college'];
			$userInfo['user_university']     = $row['user_university'];
			$userInfo['user_living_place']   = $row['user_living_place'];
			$userInfo['user_marital_status'] = $row['user_marital_status'];
			$userInfo['about_user']          = $row['about_user'];


			foreach ( $userInfo as $key => $user ) {

				$keyName = str_replace( '_', ' ', $key );
				$keyName = str_replace( 'user', ' ', $keyName );

				?>
              <div class="row editable">
                <div class="col-sm-2">
                  <b><?php echo ucwords( $keyName ); ?></b>
                </div>
                <div class="col-sm-8">
                  <span class="value"><?php echo $user; ?></span>
                  <div class="info-form">
                    <div class="input-group">
                      <input type="text" class="form-control fieldName" name="fieldName" value="">
                      <input type="hidden" name="fieldKey" class="fieldKey" value="<?php echo $key; ?>">
                      <span class="input-group-btn">
                            <button class="btn btn-secondary update" type="button">Update</button>
                          </span>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <button class="btn btn-primary edit">edit</button>
                </div>
              </div>
				<?php
			}
		}
		?>

      <!--div class="row editable">
		  <div class="col-sm-2">
			  <b>Work Place</b>
		  </div>
		  <div class="col-sm-8">
			  <span class="value">CoderGens</span>
			  <div class="info-form">
				  <div class="input-group">
					  <input type="text" class="form-control" value="">
					  <span class="input-group-btn">
					  <button class="btn btn-secondary update" type="button">Update</button>
					</span>
				  </div>
			  </div>
		  </div>
		  <div class="col-sm-2">
			  <button class="btn btn-primary edit">edit</button>
		  </div>
	  </div-->
    </div>
    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet.
    </div>
    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi assumenda consequuntur ducimus error
      exercitationem iusto nobis perspiciatis placeat suscipit.
    </div>
  </div>
</div>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/script.js"></script>

<script>


</script>


</body>

</html>
