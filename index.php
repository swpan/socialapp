<?php
session_start();
//require_once "register.php";

?> 
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="images/img2.png" height="100" width="200" style="margin-top:10px;margin-bottom:10px">
                </div>
                <div class="col-md-6">
                    <form action="login.php" method="post" enctype="multipart/form-data">
                            <div class="left">
                                <label for="email">UserName</label><br>
                                <input type="text" id="email" name="email">
                            </div>
                            <div class="left">
                                <label for="upass">UserPassword</label><br>
                                <input type="password" id="upass" name="upass"><br>
                                <a href="#">forgotten password?</a>
                            </div>
                            <div class="left">
                               <label></label><br>
                                <input type="submit" value="LogIn" name="login" class="btn btn-primary">
                            </div>
                            <div style="clear:both"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>   <!--It's header-->
    
    <div class="body">
        <div class="container">
            <div class="body-content">
                <div class="row">
                <div class="col-md-6">
                    <img src="images/img1.png" style="padding:45px 0 0 20px">
                </div>
                <div class="col-md-6">
                   <h3>Sign Up</h3>
                   <p>It's free and always will be.</p>
                    <form action="register.php" method="post" enctype="multipart/form-data" class="form2">
                       <label>First Name</label> <input type="text" name="fname"><br>
                       <label>Surname</label> <input type="text" name="sname"><br>
                       <label>Password</label> <input type="password" name="upass"><br>
                       <label>Retype Password</label> <input type="password" name="urepass"><br>
                       <label>Phone</label> <input type="text" name="phone"><br>
                       <label>Email</label> <input type="email" name="uemail"><br>
                       <label>Blood Group</label> <input type="text" name="bgroup"><br>
                       <label>Gender</label> <select name="gender">
                           <option value="-1">select</option>
                           <option value="m">Male</option>
                           <option value="f">Female</option>
                           <option value="c">Common</option>
                       </select><br>
                      <label> Birthday</label> <input type="date" name="ubirth"><br>
                       <input type="submit" value="Sign Up" name="signUp" class="btn btn-success">
                    </form>
                </div>
            </div>
            </div>
            
            
           
            
            
            
            <div class="text-center" style="padding-top:25px">All rights reserved by Swapno.</div>
        </div><!--it's body part-->
        
       
    </div>
    
    
    
   
            
                
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    
   
</body>
</html>