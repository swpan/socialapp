<?php 
session_start();
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<style>
    .home_header {
        background-color: black;
        color: white;
    }
    
    nav a::hover {
        background-color: blue;
    }
    
    .commentform {
        display: none;
    }
    
    #addPP {
        height: 350px;
        width: 100%;
        background-color: antiquewhite;
        position: absolute;
        z-index: 0;
    }
    
    #addPPP {
        height: 150px;
        width: 90%;
        background-color: blue;
        top: 110px;
        left: 0;
        position: relative;
        z-index: 10;
    }
    
    span {
        position: relative;
        left: 90%;
        z-index: 11111;
    }
    
    span.pro_pic {
        position: relative;
        left: 280px;
        display: inline;
    }

</style>

<body>

    <div class="home_header">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="images/img2.png" height="50" width="50">
                    <input type="search" value="search" size="60">
                </div>
                <div class="col-md-6">

                    <nav class="nav nav-pills flex-column flex-sm-row">
                        <a class="flex-sm-fill text-sm-center nav-link" href="profile.php">Profile</a>
                        <a class="flex-sm-fill text-sm-center nav-link" href="home.php">Home</a>
                        <a class="flex-sm-fill text-sm-center nav-link" href="logout.php">Log Out</a>
                    </nav>
                </div>


            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <div class="col-md-12">

                <!--                <p></p>-->
                <div id='ov'>
                    <div id="addPP"><img src="images/daisy.jpg" width='90%' height='350' alt=""></div>
                    <br/>
                    <!--
                <span>
                <input type="file" value="Add Profile Picture"><p></p>
                <input type="button" value="Add Cover Picture" id=''><p></p>
                </span>
-->
                </div>

                <div class='col-md-3'>
                    <div id="addPPP"></div>

                <span class='pro_pic'>
                <input type="file" value="Add Profile Picture">
                <input type="button" value="Add Profile Picture" id='action'>
                </span>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3">
                <p><a href="userAbout.php">About</a></p>
            </div>
            <div class="col-md-7"></div>
            <div class="col-md-2"></div>
            
        </div>
    </div>




    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>


    <script>
        //on("click","#action",
        $(document).ready(function() {
            $('#action').click(function() {
                var formData = new FormData();
                formData.append("profileimg", $('input[type=file]')[0].files[0]);
                formData.append("action", "#action");

                $.ajax({
                    url: 'classes/addProfilePic.php',
                    data: formData,
                    type: "POST", //ADDED THIS LINE
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        //console.log(data);
                        $("#addPPP").html(data);

                        $('input[type=file]').val('');
                    }
                });
            });
        });

    </script>


</body>

</html>
